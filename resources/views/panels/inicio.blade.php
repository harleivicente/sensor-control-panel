<!-- 
	Variáveis:

		$error
		$tipos_sensores - Medidas recentes para, no máximo, três tipos de sensores no formato:

		[
			[
				"tipo" => "temperatura",
				"valores" => [
					("id" => 1,
					"value" => 23.4), ...
				]
			]
			...
		]
 -->

@extends('layouts.main')

@section('titulo')
	Inicio
@endsection

@section('content')
    
	@if(isset($error))
    	<div class="alert alert-danger" role="alert"> {{$error}} </div>
    @endif

	<div class="row">
		@foreach ($tipos_sensores as $tipo_sensor)
			<div class="col-md-3">
				
				<div class="panel panel-default">
				  <div class="panel-heading">
				    <h3 class="panel-title">{{$tipo_sensor['tipo']}}</h3>
				  </div>
				  <div class="panel-body">

					<table class="table">
					  	<thead>
					  		<tr>
					  			<th>
					  				Codigo
					  			</th>
					  			<th>
					  				Valor
					  			</th>
					  		</tr>
					  	</thead>
					  	<tbody>
						  	@for($i = 0; $i < count($tipo_sensor['valores']); $i++)
					  		<tr>
								<td> {{$tipo_sensor['valores'][$i]->id}} </td>
								<td> {{$tipo_sensor['valores'][$i]->value}} </td>
					  		</tr>
							@endfor
					  	</tbody>
					</table>

				  </div>
				</div>

			</div>
	    @endforeach
	</div>

@endsection