<!-- 
	Variáveis:

		$error
 -->

@extends('layouts.main')

@section('titulo')
	Erro de licença
@endsection

@section('content')
    
	@if(isset($error))
    	<div class="alert alert-danger" role="alert"> {{$error}} </div>
    @endif

	<div class="row">
	</div>

@endsection