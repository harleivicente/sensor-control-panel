<!-- 

Variáveis
	form_error
		generic - Não foi possível alterar a distribuição
	ambientes - array de Ambiente
	equipamentos - array de Equipamento
	sensores - array de Sensor
	distribuicao - Distribuicao que está sendo editada
 -->

@extends('layouts.main')

@section('titulo')
	Distribuição de sensores
@endsection

@section('content')


<div class="row">
	<div class="col-lg-12">

		@if($form_error['generic']) <div class="alert alert-danger" role="alert"> Não foi possível criar essa configuração.</div> @endif

		<div class="panel panel-default">
		  <div class="panel-heading">
		    <h3 class="panel-title">Editar configuração</h3>
		  </div>
		  <div class="panel-body">
				
				<form action="{{Request::root()}}/config/distribuicao" method="post">
				    {{ csrf_field() }}

					<div class="form-group">
						<label class="control-label" for="distAmbiente"> Ambiente </label>
						<select class="form-control" name="distAmbiente" id="distAmbiente">
							@foreach ($ambientes as $ambiente)
								<option value="{{$ambiente->id_ambiente}}" {{$ambiente->id_ambiente === $distribuicao->id_ambiente ? "selected" : ""}} > {{$ambiente->desc_nome}}</option>
							@endforeach
						</select>
					</div>
					
					<div class="form-group">
						<label class="control-label" for="distEquipamento"> Equipamento </label>
						<select class="form-control" name="distEquipamento" id="distEquipamento">
							@foreach ($equipamentos as $equipamento)
								<option value="{{$equipamento->id_equipamento}}" {{$equipamento->id_equipamento === $distribuicao->id_equipamento ? "selected" : ""}}> {{$equipamento->desc_nome}} / {{$equipamento->desc_codigo}} </option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label class="control-label" for="distSensor"> Tipo de sensor </label>
						<select class="form-control" name="distSensor" id="distSensor">
							@foreach ($sensores as $sensor)
								<option value="{{$sensor->id_sensor}}" {{$sensor->id_sensor === $distribuicao->id_sensor ? "selected" : ""}} > {{$sensor->desc_nome}} / {{$sensor->desc_sigla}} </option>
							@endforeach
						</select>
					</div>
					
					<input type="hidden" name="distId" id="distId" value="{{$distribuicao->id_sensorambienteequipamento}}"/>
					<button type="submit" class="btn btn-default">Salvar</button>
				</form>

		  </div>
		</div>
	</div>
</div>

@endsection