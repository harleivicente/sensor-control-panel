<!-- 

Variáveis:
	configuracoes: array de std com campos:
		id | ambiente_nome | equipamento_nome | equipamento_codigo | sensor_nome | sensor_sigla
 -->

@extends('layouts.main')

@section('titulo')
	<div class="row">
		<div class="col-md-8">
		Distribuição de sensores
		</div>
		<div class="col-md-2 col-md-offset-2">
			 <a href="{{Request::root()}}/config/distribuicao/adicionar" class="btn btn-success" role="button">Criar configuração</a>
		</div> 
	</div>
@endsection

@section('content')
	

	<div class="row">
		<div class="col-lg-12">
			  
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">Configurações existentes</h3>
			  </div>
			  <div class="panel-body">

				<table class="table">
				  	<thead>
				  		<tr>
				  			<th>
				  				Ambiente
				  			</th>
				  			<th>
				  				Equipamento
				  			</th>
				  			<th>
				  				Tipo de sensor
				  			</th>
				  			<th>
				  				Ações
				  			</th>
				  		</tr>
				  	</thead>
				  	<tbody>
					  	@foreach ($configuracoes as $configuracao)
				  		<tr>
							<td> {{$configuracao->ambiente_nome}} </td>
							<td> {{$configuracao->equipamento_nome}} - {{$configuracao->equipamento_codigo}} </td>
							<td> {{$configuracao->sensor_nome}} - {{$configuracao->sensor_sigla}} </td>
				  			<td>
				  				<a href="{{Request::root()}}/config/distribuicao/remover/{{$configuracao->id}}"> Deletar </a>
				  				<a href="{{Request::root()}}/config/distribuicao/editar/{{$configuracao->id}}"> Editar </a>
				  			</td>
				  		</tr>
						@endforeach
				  	</tbody>
				</table>

			  </div>
			</div>

		</div>
	</div>

@endsection

