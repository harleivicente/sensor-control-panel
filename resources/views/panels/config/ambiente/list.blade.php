@extends('layouts.main')

@section('titulo')
	<div class="row">
		<div class="col-md-8">
		Configuração de ambientes
		</div>
		<div class="col-md-2 col-md-offset-2">
			 <a href="{{Request::root()}}/config/ambiente/adicionar" class="btn btn-success" role="button">Adicionar</a>
		</div> 
	</div>
@endsection

@section('content')
	

	<div class="row">
		<div class="col-lg-12">
			  
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">Ambientes existentes</h3>
			  </div>
			  <div class="panel-body">

				<table class="table">
				  	<thead>
				  		<tr>
				  			<th>
				  				Nome
				  			</th>
				  			<th>
				  				Ações
				  			</th>
				  		</tr>
				  	</thead>
				  	<tbody>
					  	@foreach ($ambientes as $ambiente)
				  		<tr>
							<td> {{$ambiente->desc_nome}} </td>
				  			<td>
				  				<a href="{{Request::root()}}/config/ambiente/remover/{{$ambiente->id_ambiente}}"> Deletar </a>
				  				<a href="{{Request::root()}}/config/ambiente/editar/{{$ambiente->id_ambiente}}"> Editar </a>
				  			</td>
				  		</tr>
						@endforeach
				  	</tbody>
				</table>

			  </div>
			</div>

		</div>
	</div>

@endsection

