<!-- 

Errors
	
	form_error
		nome - Nome não pode ser vazio

 -->

@extends('layouts.main')

@section('titulo')
	Configuração de ambientes
@endsection

@section('content')
	

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">Adicionar ambiente</h3>
			  </div>
			  <div class="panel-body">
					
					<form action="{{Request::root()}}/config/ambiente" method="post">
					    {{ csrf_field() }}

						<div class="form-group @if($form_error['nome']) has-error @endif">
							<label class="control-label" for="ambienteNome"> Nome </label>
							<input type="text" class="form-control" name="ambienteNome" id="ambienteNome", placeholder="Nome do ambiente">
							@if($form_error['nome'])
								<span class="help-block"> Nome não pode ser vazio. </span>
							@endif
						</div>
						<button type="submit" class="btn btn-default">Salvar</button>
					</form>

			  </div>
			</div>
		</div>
	</div>

@endsection