<!-- 
	Variáveis:

		$error
		$ok
 -->

@extends('layouts.main')

@section('titulo')
	Licença
@endsection

@section('content')

	<div class="row">

		<div class="col-lg-12">
			
			@if(isset($ok))
	        	<div class="alert alert-success" role="alert"> Licença atualizada. </div>
	        @endif

			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">Atualizar licença</h3>
			  </div>
			  <div class="panel-body">
					
					<form action="{{Request::root()}}/config/licenca" method="post">
					    {{ csrf_field() }}

						<div class="form-group @if($error) has-error @endif">
							<label class="control-label" for="licenca"> Codigo </label>
							<input type="text" class="form-control" name="licenca" id="licenca">
							@if($error)
								<span class="help-block"> Licença deve ter no máximo 20 characteres. </span>
							@endif
						</div>
						<button type="submit" class="btn btn-default">Salvar</button>
					</form>

			  </div>
			</div>
		</div>

	</div>

@endsection


	

