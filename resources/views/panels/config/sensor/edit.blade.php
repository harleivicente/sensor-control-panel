<!-- 

Errors
	
	form_error
		nome - Nome não pode ser vazio
		sigla - Sigla não pode pode ser repetido. Limite de 2 letras.

 -->

@extends('layouts.main')

@section('titulo')
	Configuração de sensores
@endsection

@section('content')


<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
		  <div class="panel-heading">
		    <h3 class="panel-title">Editar sensor</h3>
		  </div>
		  <div class="panel-body">
				
				<form action="{{Request::root()}}/config/sensor" method="post">
				    {{ csrf_field() }}

					<div class="form-group @if($form_error['nome']) has-error @endif">
						<label class="control-label" for="sensorNome"> Nome </label>
						<input type="text" class="form-control" name="sensorNome" id="sensorNome" value="{{$sensor->desc_nome}}">
						@if($form_error['nome'])
							<span class="help-block"> Nome não pode ser vazio. </span>
						@endif
					</div>
					
					<div class="form-group @if($form_error['sigla']) has-error @endif">
						<label class="control-label" for="sensorSigla"> Sigla </label>
						<input type="text" class="form-control" name="sensorSigla" id="sensorSigla" value="{{$sensor->desc_sigla}}" placeholder="Sigla do sensor">
						@if($form_error['sigla'])
							<span class="help-block"> Sigla deve conter entre 1 e 2 caracteres. Deve ser único. </span>
						@endif
					</div>
					
					<input type="hidden" class="form-control" name="id_sensor" id="id_sensor" value="{{$sensor->id_sensor}}">
					<button type="submit" class="btn btn-default">Salvar</button>
				</form>

		  </div>
		</div>
	</div>
</div>

@endsection