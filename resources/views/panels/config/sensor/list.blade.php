@extends('layouts.main')

@section('titulo')
	<div class="row">
		<div class="col-md-8">
		Configuração de sensores
		</div>
		<div class="col-md-2 col-md-offset-2">
			 <a href="{{Request::root()}}/config/sensor/adicionar" class="btn btn-success" role="button">Adicionar</a>
		</div> 
	</div>
@endsection

@section('content')
	

	<div class="row">
		<div class="col-lg-12">
			  
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">Sensores existentes</h3>
			  </div>
			  <div class="panel-body">

				<table class="table">
				  	<thead>
				  		<tr>
				  			<th>
				  				Nome
				  			</th>
				  			<th>
				  				Sigla
				  			</th>
				  			<th>
				  				Ações
				  			</th>
				  		</tr>
				  	</thead>
				  	<tbody>
					  	@foreach ($sensores as $sensor)
				  		<tr>
							<td> {{$sensor->desc_nome}} </td>
							<td> {{$sensor->desc_sigla}} </td>
				  			<td>
				  				<a href="{{Request::root()}}/config/sensor/remover/{{$sensor->id_sensor}}"> Deletar </a>
				  				<a href="{{Request::root()}}/config/sensor/editar/{{$sensor->id_sensor}}"> Editar </a>
				  			</td>
				  		</tr>
						@endforeach
				  	</tbody>
				</table>

			  </div>
			</div>

		</div>
	</div>

@endsection

