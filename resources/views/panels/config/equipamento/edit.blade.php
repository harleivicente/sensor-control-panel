<!-- 

Errors
	
	form_error
		nome - Nome não pode ser vazio
		codigo - Codigo não pode pode ser repetido. Limite de 8 letras.

 -->

@extends('layouts.main')

@section('titulo')
	Configuração de equipamentos
@endsection

@section('content')


<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
		  <div class="panel-heading">
		    <h3 class="panel-title">Editar equipamento</h3>
		  </div>
		  <div class="panel-body">
				
				<form action="{{Request::root()}}/config/equipamento" method="post">
				    {{ csrf_field() }}

					<div class="form-group @if($form_error['nome']) has-error @endif">
						<label class="control-label" for="equipamentoNome"> Nome </label>
						<input type="text" class="form-control" name="equipamentoNome" id="equipamentoNome" value="{{$equipamento->desc_nome}}">
						@if($form_error['nome'])
							<span class="help-block"> Nome não pode ser vazio. </span>
						@endif
					</div>
					
					<div class="form-group @if($form_error['codigo']) has-error @endif">
						<label class="control-label" for="equipamentoCdigo"> Codigo </label>
						<input type="text" class="form-control" name="equipamentoCodigo" id="equipamentoCodigo" value="{{$equipamento->desc_codigo}}" placeholder="Codigo do equipamento">
						@if($form_error['codigo'])
							<span class="help-block"> Codigo deve conter entre 1 e 8 caracteres. Deve ser único. </span>
						@endif
					</div>
					
					<input type="hidden" class="form-control" name="id_equipamento" id="id_equipamento" value="{{$equipamento->id_equipamento}}">
					<button type="submit" class="btn btn-default">Salvar</button>
				</form>

		  </div>
		</div>
	</div>
</div>

@endsection