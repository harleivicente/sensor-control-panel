<!-- 
Variáveis:

	$usuarios array de usuários
	$error - Mensagem de erro

 -->

@extends('layouts.main')

@section('titulo')
	<div class="row">
		<div class="col-md-8">
		Configuração de usuários
		</div>
		<div class="col-md-2 col-md-offset-2">
			 <a href="{{Request::root()}}/config/usuario/adicionar" class="btn btn-success" role="button">Adicionar</a>
		</div> 
	</div>
@endsection

@section('content')
	

	<div class="row">
		<div class="col-lg-12">
			@if(isset($error))
            	<div class="alert alert-danger" role="alert"> {{$error}} </div>
            @endif
			  
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">Usuários existentes</h3>
			  </div>
			  <div class="panel-body">

				<table class="table">
				  	<thead>
				  		<tr>
				  			<th>
				  				Usuário
				  			</th>
				  			<th>
				  				Tipo
				  			</th>
				  			<th>
				  				Ações
				  			</th>
				  		</tr>
				  	</thead>
				  	<tbody>
					  	@foreach ($usuarios as $usuario)
				  		<tr>
							<td> {{$usuario->username}} </td>
							@if ($usuario->type === '0')
								<td> Root </td>
							@elseif ($usuario->type === '1')
								<td> Administrador </td>
							@else
								<td> Regular </td>
							@endif
				  			<td>
				  				<a href="{{Request::root()}}/config/usuario/remover/{{$usuario->id_seglogin}}"> Deletar </a>
				  				<a href="{{Request::root()}}/config/usuario/editar/{{$usuario->id_seglogin}}"> Editar </a>
				  			</td>
				  		</tr>
						@endforeach
				  	</tbody>
				</table>

			  </div>
			</div>

		</div>
	</div>

@endsection

