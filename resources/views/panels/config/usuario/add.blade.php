<!-- 

Variáveis
	
	form_error
		username
		password
	types - tipos de usuários que podem ser atribuidos pelo usuário logado. Array de:
		[codigo => 1, texto => 'Administrador']

 -->

@extends('layouts.main')

@section('titulo')
	Configuração de usuários
@endsection

@section('content')


<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
		  <div class="panel-heading">
		    <h3 class="panel-title">Criar usuário</h3>
		  </div>
		  <div class="panel-body">
				
				<form action="{{Request::root()}}/config/usuario" method="post">
				    {{ csrf_field() }}

					<div class="form-group @if($form_error['username']) has-error @endif">
						<label class="control-label" for="username"> Usuário </label>
						<input type="text" class="form-control" name="username" id="username">
						@if($form_error['username'])
							<span class="help-block"> {{$form_error['username']}} </span>
						@endif
					</div>
					
					<div class="form-group @if($form_error['password']) has-error @endif">
						<label class="control-label" for="password"> Password </label>
						<input type="password" class="form-control" name="password" id="password">
						@if($form_error['password'])
							<span class="help-block"> {{$form_error['password']}} </span>
						@endif
					</div>

					<div class="form-group">
						<label class="control-label" for="type"> Tipo de usuário </label>
						<select class="form-control" name="type" id="type">
							@foreach ($types as $type)
								<option value={{$type['codigo']}}> {{$type['texto']}}</option>
							@endforeach
						</select>
					</div>
					
					<button type="submit" class="btn btn-default">Salvar</button>
				</form>

		  </div>
		</div>
	</div>
</div>

@endsection