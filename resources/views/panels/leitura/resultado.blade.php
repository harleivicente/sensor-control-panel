<!-- 
	Variáveis:

		$dados - array de items no formato:
			[
				'distribuicao' => stdClass da distribuicao com campos:
					id | ambiente_nome | equipamento_nome | equipamento_codigo | sensor_nome | sensor_sigla
				'medicoes' - Array de stdClass com atributos
						- tempo
						- valor
			]
			
 -->

@extends('layouts.main')

@section('titulo')
	Consultar leitura de sensores
@endsection

@section('content')

	<!-- LISTAGEM -->
		<div class="row">

			@foreach ($dados as $dado)

				<div class="col-md-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>#{{$dado['distribuicao']->id}} - {{$dado['distribuicao']->sensor_nome}}</h4>
							<p>{{$dado['distribuicao']->ambiente_nome}} - {{$dado['distribuicao']->equipamento_nome}}</p>
						</div>
						<div class="panel-body">
							<table class="table">
							  	<thead>
							  		<tr>
							  			<th>
							  				Data e horário
							  			</th>
							  			<th>
							  				Valor
							  			</th>
							  		</tr>
							  	</thead>
							  	<tbody>
							  	@for($i = 0; $i < count($dado['medicoes']); $i++)
							  		<tr>
							  			<td>
							  				{{$dado['medicoes'][$i]->tempo}}
							  			</td>
							  			<td>
							  				{{$dado['medicoes'][$i]->valor}}
							  			</td>
							  		</tr>
						  		@endfor
							  	</tbody>
							</table>
						</div>
					</div>
				</div>



			@endforeach

		</div>




@endsection
