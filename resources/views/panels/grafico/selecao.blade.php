<!-- 
	Variáveis:

		$error

		$ambientes - lista de ambientes
		$equipamentos - lista de equipamentos
		$sensores - lista de sensores

		$ambienteId - ambiente selecionado no filtro
		$equipamentoId
		$sensorId

		$distribuicoes - lista de instancias de sensores com campos:
			id | ambiente_nome | equipamento_nome | equipamento_codigo | sensor_nome | sensor_sigla
 -->

@extends('layouts.main')

@section('titulo')
	Consultar graficos
@endsection

@section('content')

	@if(isset($error))
    	<div class="alert alert-danger" role="alert"> {{$error}} </div>
    @endif

    <!-- FILTROS -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Filtros</h3>
		</div>
		<div class="panel-body">
		  	<form action="{{Request::root()}}/leitura/filtrar" method="post">
		    	{{ csrf_field() }}

			  	<div class="row">
			  		<div class="col-md-6">
						<div class="form-group">
							<label class="control-label" for="filterAmbiente"> Ambiente </label>
							<select class="form-control" name="filterAmbiente" id="filterAmbiente">
									<option> Selecione um item </option>
								@foreach ($ambientes as $ambiente)
									<option value="{{$ambiente->id_ambiente}}" {{$ambiente->id_ambiente === $ambienteId ? "selected" : ""}} > {{$ambiente->desc_nome}}</option>
								@endforeach
							</select>
						</div>
				
						<div class="form-group">
							<label class="control-label" for="filterEquipamento"> Equipamento </label>
							<select class="form-control" name="filterEquipamento" id="filterEquipamento">
									<option> Selecione um item </option>
								@foreach ($equipamentos as $equipamento)
									<option value="{{$equipamento->id_equipamento}}" {{$equipamento->id_equipamento === $equipamentoId ? "selected" : ""}} > {{$equipamento->desc_nome}}</option>
								@endforeach
							</select>
						</div>

			  		</div>

			  		<div class="col-md-6">

						<div class="form-group">
							<label class="control-label" for="filterSensor"> Sensor </label>
							<select class="form-control" name="filterSensor" id="filterSensor">
									<option> Selecione um item </option>
								@foreach ($sensores as $sensor)
									<option value="{{$sensor->id_sensor}}" {{$sensor->id_sensor === $sensorId ? "selected" : ""}} > {{$sensor->desc_nome}}</option>
								@endforeach
							</select>
						</div>

						<div class="row" style="padding-top: 25px;">
							@if($ambienteId || $equipamentoId || $sensorId)
								<div class="col-md-4">
									<a class="btn btn-default" style="width:100%;" href="{{Request::root()}}/leitura/limpar_filtro">Limpar</a>
								</div>
							@endif
							<div class="col-md-4">
								<button type="submit" style="width:100%;" class="btn btn-success">Filtrar</button>
							</div>
						</div>

			  		</div>
		  		</div>

		  	</form>
		</div>
	</div>

<form class="form-inline" action="{{Request::root()}}/grafico/resultado" method="post">
	{{ csrf_field() }}

	<!-- LISTAGEM -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Sensores</h3>
		</div>
		<div class="panel-body">

		  	
				<table class="table">
				  	<thead>
				  		<tr>
				  			<th style="width:100px;">
				  				Seleção
				  			</th>
				  			<th>
				  				Codigo do sensor
				  			</th>
				  			<th>
				  				Ambiente
				  			</th>
				  			<th>
				  				Equipamento
				  			</th>
				  			<th>
				  				Tipo de sensor
				  			</th>				  							  							  			
				  		</tr>
				  	</thead>
				  	<tbody>
				  	@foreach($distribuicoes as $distribuicao)
				  		<tr>
							<td>
								<div class="checkbox">
									<input type="checkbox" name="distribuicoesId[]" value="{{$distribuicao->id}}">
								</div>
							</td>
				  			<td>
				  				{{$distribuicao->id}}
				  			</td>
				  			<td>
				  				{{$distribuicao->ambiente_nome}}
				  			</td>
				  			<td>
				  				{{$distribuicao->equipamento_nome}}
				  			</td>
				  			<td>
				  				{{$distribuicao->sensor_nome}}
				  			</td>				  							  							  			
				  		</tr>
			  		@endforeach
				  	</tbody>
				</table>
			
		</div>
	</div>


	<!-- CONFIGURAÇÕES DE RESULTADOS -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Configurações dos resultados</h3>
		</div>
		<div class="panel-body">
		  	<div class="row">

				<div class="col-md-3">
					<div class="form-group">
				      		<label>
    							<input type="checkbox" value='true' id="tempoReal" name="tempoReal">
    							Tempo real
  							</label>
				    </div>
				</div>

		  		<div class="col-md-3">
					<div class="form-group">
						<label class="control-label" for="filterAmbiente"> Data inicial </label>
						<div class='input-group date datetimepicker' id='datetimepickerInicio' name="dataInicio">
		                    <input type='text' class="form-control"/>
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>
					</div>
		  		</div>
		  		
		  		<div class="col-md-3">
					<div class="form-group">
						<label class="control-label" for="filterAmbiente"> Data final </label>
						<div class='input-group date datetimepicker' id='datetimepickerFim' name='dataFim'>
		                    <input type='text' class="form-control"/>
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>
					</div>						
		  		</div>


				<div class="col-md-3">
					<div class="form-group" style="width:100%;">
						<button type="submit" style="float:right; margin-top:20px;" class="btn btn-success">Gerar dados</button>
					</div>
				</div>

			</div>
	</div>

	
	<!-- DATA INPUTS -->
	<input type="hidden" name="dataInicio" id="dataInicio"/>
	<input type="hidden" name="dataFim" id="dataFim"/>
</form>

@endsection

@section('js')
    <script type="text/javascript">
        $(function () {

        	// Configurar datepickers
            $('.datetimepicker').each(function(i, item){
            	$(item).datetimepicker({
                locale: 'pt-br'});

	            $(item).on('dp.change', function(e){
	            	if(e.date) {
		            	var dateString = e.date.format("YYYY-MM-DD HH:mm:ss");
		            	var nome = $(item).attr('name');
		            	$("#" + nome).val(dateString);
	            	}
	            });
            });

            // Configurar toggle tempo real
            $("#tempoReal").change(function(event){
            	var checked = $(event.target).prop('checked');
            	
            	if(checked){
            		$('#datetimepickerInicio input').prop('disabled', true);
            		$('#datetimepickerFim input').prop('disabled', true);
            		$('#datetimepickerInicio').data('DateTimePicker').clear();
            		$('#datetimepickerFim').data('DateTimePicker').clear();
            		$('#dataInicio').val(null);
            		$('#dataFim').val(null);
            	} else {
            		$('#datetimepickerInicio input').prop('disabled', false);
            		$('#datetimepickerFim input').prop('disabled', false);
            	}
            });
        });
    </script>
@endsection					      