<!-- 
	Variáveis:

		$dados - array de items no formato:
			[
				'tipo' => Tipo de sensor (i.e Temperatura)
				'medicoes' - Array de:
					['codigo' => 2, values =>[valor, ...]]
			]
		
		$labels - Array de labels para os gráficos (12 items)

		$tempoReal - boolean - Grafico de ser atualizado automaticamente
 -->

@extends('layouts.main')

@section('titulo')
	Consultar gráficos {{$tempoReal ? ' - Tempo real' : ""}}
@endsection

@section('content')

	<!-- Graficos -->

		@foreach ($dados as $dado)
			<div class="panel panel-default">
				<div class="panel-heading">
					{{$dado['tipo']}}
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<canvas class="charts" style="width:100%; height: 200px;" smca-data="{{json_encode($dado['medicoes'])}}"></canvas>
						</div>
					</div>
				</div>
			</div>
		@endforeach

		<input type="hidden" id="smca-url" value="{{ Request::getBaseUrl() }}" />
		<input type="hidden" id="smca-labels" value="{{json_encode($labels)}}" />
		<input type="hidden" id="smca-tempoReal" value="{{$tempoReal ? 'true' : ''}}" />

@endsection


@section('js')
<script>
	/*
		Relaciona os id's dos sensores e os charts nos quais estão desenhados.
		Array de items no formato:
		{
			sensorIds: [1,2,3,...],
			chartInstance: <instancia de chart>
		}
	*/
	gSensores = [];
	gBaseUrl = null;

	/*
		Função responsável por atualizar todos os datasets de todos os gráficos.
	*/
	function atualizarGraficos() {
		var sensorIds = [];
		var dadosServidor = [];
		var counter  = -1; // Indica qual dataset está sendo processado em um chart (0 - n)
		var datasetAtual = 0; // Numero total de datasets que já foram processados
		var numeroPontosNovos = null;
		var numeroPontosOriginais = null;

		// Coletar ids dos sensores
		gSensores.forEach(function(chart) {
			chart.sensorIds.forEach(function(sensorId, index){
				sensorIds.push(sensorId);
			})
		});

		// Obter dados do servidor
		$.ajax({
		  method: "GET",
		  async: false,
		  url: gBaseUrl + "/grafico/sensor/" + JSON.stringify(sensorIds)
		})
		  .done(function( dados ) {
		  	dadosServidor = JSON.parse(dados);
	  	});

		// Calcular numero de pontos novos
		numeroPontosNovos = dadosServidor[0].length - gSensores[0].chartInstance.datasets[0].points.length;
		numeroPontosOriginais = gSensores[0].chartInstance.datasets[0].points.length;

		gSensores.forEach(function(chart) {

			// Atualizar pontos existentes com novos valores
			chart.chartInstance.datasets.forEach(function(dataset, index){
				counter++;

				// Atualizar pontos que já existem
				for(var i = 0; i < dataset.points.length; i++){
					dataset.points[i].value = dadosServidor[counter][i].valor;
				}
			})

			// Redesenhar grafico
			chart.chartInstance.update();

			// Adicionar pontos excedentes
			for(var i = numeroPontosOriginais; i < (numeroPontosOriginais + numeroPontosNovos); i++){
				var valoresDeCadaDataset = [];

				for(var b = 0; b < dadosServidor.length; b++){
					
					// Considerar apenas datasets que pertencem ao chart
					if(b < datasetAtual || b >= (chart.chartInstance.datasets.length + datasetAtual)) {
						continue;
					}

					valoresDeCadaDataset.push(dadosServidor[b][i].valor);
				}

				// Inserir novos pontos	
				chart.chartInstance.addData(valoresDeCadaDataset, dadosServidor[0][i].tempo);
				
			}

			// Atualizar numero de datasets processados
			datasetAtual += chart.chartInstance.datasets.length;

			// Atualizar labels
			novosLabels = [];
			for(var b = 0; b < dadosServidor[0].length; b++){
				novosLabels.push(dadosServidor[0][b].tempo);
			}
			chart.chartInstance.scale.xLabels = novosLabels;
			chart.chartInstance.update();

		});
	}



	$(function(){
		gBaseUrl = $("#smca-url").val();

		// Ativar os gráficos
		$(".charts").each(function(i, item){	
			var dataset_data = JSON.parse($(item).attr('smca-data'));

			// Cria objeto para colocar na global gSensores
			var sensoresData = {sensorIds: [], chartInstance: null};

			var data = {
			    labels: JSON.parse($("#smca-labels").val()),
			    datasets: []
			};

			for (var i = 0; i < dataset_data.length; i++) {

				// Registra o id do sensor
				sensoresData.sensorIds.push(dataset_data[i]['codigo']);

				var newDataset = {		        
		            label: "#" + dataset_data[i]['codigo'],
		            fillColor: "rgba(220,220,220,0.2)",
		            strokeColor: "rgba(220,220,220,1)",
		            pointColor: "rgba(220,220,220,1)",
		            pointStrokeColor: "#fff",
		            pointHighlightFill: "#fff",
		            pointHighlightStroke: "rgba(220,220,220,1)",
		            data: null
       			};
       			newDataset.data = dataset_data[i]['values'];
       			data.datasets.push(newDataset);
			};

			var newChart = new Chart(item.getContext("2d")).Line(data, {
				multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",
			});

			sensoresData.chartInstance = newChart;
			gSensores.push(sensoresData);
		});

		// Configurar loop de tempo real
		if($("#smca-tempoReal").val()){
			setInterval(function(){
				atualizarGraficos();
			}, 1000);
		}

	});

</script>
@endsection