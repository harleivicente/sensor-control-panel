<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SMCA</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ Request::getBaseUrl() }}/vendor/sb-admin/css/bootstrap.min.css" rel="stylesheet">

    <style type="text/css">
        .vertical-center {
          min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
          min-height: 100vh; /* These two lines are counted as one :-)       */

          display: flex;
          align-items: center;
        }

        #container.container {
            max-width: 400px;
        }     

    </style>
</head>

<body>

    @yield('content')

    <!-- jQuery -->
    <script src="{{ Request::getBaseUrl() }}/vendor/sb-admin/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ Request::getBaseUrl() }}/vendor/sb-admin/js/bootstrap.min.js"></script>

</body>

</html>
