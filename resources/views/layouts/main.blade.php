<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SMCA</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ Request::getBaseUrl() }}/vendor/sb-admin/css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ Request::getBaseUrl() }}/vendor/sb-admin/css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ Request::getBaseUrl() }}/vendor/sb-admin/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ Request::getBaseUrl() }}">SMCA</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{Auth::user()->username}} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ Request::getBaseUrl() }}/auth/logout"><i class="fa fa-fw fa-power-off"></i> Sair </a>
                        </li>
                    </ul>
                </li>
            </ul>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="{{ Request::path() === '/' ? 'active' : '' }}">
                        <a href="{{ Request::getBaseUrl() }}"><i class="fa fa-fw fa-dashboard"></i> Inicio </a>
                    </li>
                    <li>
                        <a href="{{ Request::getBaseUrl() }}/leitura"><i class="fa fa-fw fa-table"></i> Leitura </a>
                    </li>

                    @if($licenca_grafico)
                        <li>
                            <a href="{{Request::getBaseUrl()}}/grafico"><i class=" fa fa-fw fa-bar-chart-o"></i> Gráficos </a>
                        </li>
                    @endif

                    @if($request->user()->canConfig() && $licenca_carta)
                        <li>
                            <a href="{{ Request::getBaseUrl() }}/carta"><i class="fa fa-fw fa-edit"></i> Carta-controle </a>
                        </li>
                    @endif

                    @if($request->user()->canConfig())
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Configurações <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="{{ Request::getBaseUrl() }}/config/ambientes">Ambientes</a>
                                </li>
                                <li>
                                    <a href="{{ Request::getBaseUrl() }}/config/equipamentos">Equipamentos</a>
                                </li>
                                <li>
                                    <a href="{{ Request::getBaseUrl() }}/config/sensores">Sensores</a>
                                </li>
                                <li>
                                    <a href="{{ Request::getBaseUrl() }}/config/distribuicao">Distribuição</a>
                                </li>
                                <li>
                                    <a href="{{ Request::getBaseUrl() }}/config/outliers">Outliers</a>
                                </li>
                                <li>
                                    <a href="{{ Request::getBaseUrl() }}/config/usuarios">Usuários</a>
                                </li>
                                <li>
                                    <a href="{{ Request::getBaseUrl() }}/config/licenca">Licença</a>
                                </li>                           
                            </ul>
                        </li>
                    @endif

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            @yield('titulo')
                        </h1>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        @yield('content')
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ Request::getBaseUrl() }}/vendor/sb-admin/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ Request::getBaseUrl() }}/vendor/sb-admin/js/bootstrap.min.js"></script>

    <!-- CHARTJS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js" type="text/javascript"></script>
    
    <!-- MOMENT.JS -->
    <script type="text/javascript" src="{{ Request::getBaseUrl() }}/vendor/moment.min.js"></script>

    <!-- BOOTSTRAP DATETIME PICKER -->
    <script type="text/javascript" src="{{ Request::getBaseUrl() }}/vendor/bootstrap-datetimepicker.js"></script>

    <!-- CUSTOM JS -->
    @yield('js')
    
</body>

</html>
