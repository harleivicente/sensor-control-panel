

@extends('layouts.login')


@section('content')

<div class="jumbotron vertical-center" style="background-color: #333;"> 
    <div id="container" class="container">

        <div class="panel panel-default">

            <div class="panel-heading">
                <h3 class="panel-title"> Log in </h3>
            </div>

            <div class="panel-body">
                <!-- <div class="alert alert-danger" role="alert"> Não foi possível criar essa configuração.</div> -->
                
                <form method="POST" action="{{Request::root()}}/auth/login">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label class="control-label" for="username"> Email </label>
                        <input class="form-control" type="text" name="username" id="username">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="password"> Password </label>
                        <input class="form-control" type="password" name="password" id="password">
                    </div>


                    <button type="submit" class="btn btn-default"> Login </button>
                </form>
            </div>
        </div>

    </div>
</div>

@endsection