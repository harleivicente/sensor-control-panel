<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use stdClass;

class Leitura extends Model
{
	protected $table = 'leitura';
	protected $primaryKey = 'id_leitura';


	/*
		Obtem o valor mais recente para cada sensor de um dado tipo.

		@return - Formato: array de objetos:
			'id' => 1,
			'value' => 21

	*/
	public static function obterValoresRecentesPorTipoSensor ($tipo_sensor_id) {
        $sub = DB::table('leitura')
        ->orderBy('id_sensorambienteequipamento', 'asc')
        ->orderBy('data_inclusao', 'desc')
        ->orderBy('id_leitura', 'asc');

	    $query = DB::table(DB::raw("({$sub->toSql()}) as sub") )
        ->leftJoin('sensor_ambiente_equipamento', 'sub.id_sensorambienteequipamento', '=', 'sensor_ambiente_equipamento.id_sensorambienteequipamento')
        ->selectRaw('sub.id_sensorambienteequipamento as id, sub.valor as value')
        ->where('sensor_ambiente_equipamento.id_sensor', $tipo_sensor_id)
        ->groupBy('sub.id_sensorambienteequipamento');

        return $query->get();
	}


    /*
        Alinha em relação ao tempo de medição medidas de vários sensores diferentes.
        Os items dos array devem estar ordenados em ordem crescente. (ultimo item do array deve
        ser o mais recente.) Por exemplo:

        Seja 
            A, B, C - tres sensores distintos
            An, Bn, Cn medições tomadas pelo sensor no instante 'n' segundos

        Os seguinte dados:
            A1, A2, A3
            B2
            C2, C3
        
        Seriam alinhados assim:
            A1,     A2,     A3
            null,   B2,     null
            null,   C2,     C3

        @param $medidasSensores - array de:
            [
                stdClass
                    - tempo <string no formato : 2016-01-04 00:00:00>
                    - valor <int>
                    - timestamp <unix timestamp>
                ,
                ...
            ]

        @return - array de:
            [
                stdClass
                    - tempo <string no formato : 2016-01-04 00:00:00>
                    - valor <int>
                    - timestamp <unix timestamp>
                ,
                ...
            ]            

    */
    public static function alinharMedidasSensores ($medidasSensores) {
        $leftmostTs = 0;

        while(true){
            
            // Encontrar proximo timestamp para processar
            $nextTs = -1;
            $nextLabel = "";
            foreach($medidasSensores as $medidasSensor){
                foreach($medidasSensor as $medidaSensor) {
                    if($medidaSensor->timestamp > $leftmostTs && ($nextTs < 0 || $medidaSensor->timestamp < $nextTs)){
                        $nextTs = $medidaSensor->timestamp;
                        $nextLabel = $medidaSensor->tempo;
                    }
                }
            }

            // Se não houver proximo timestamp(ts), alinhamento terminado
            if($nextTs < 0){
                break;
            }

            // Inserir items vazios para o ts sendo processado
            foreach($medidasSensores as $medidasSensorKey => $medidasSensor){

                // Encontrar posição para inserir item null
                $insertKey = 0;
                foreach($medidasSensor as $medidaSensorKey => $medidaSensor){
                    if($medidaSensor->timestamp == $nextTs){
                        $insertKey = -1;
                        break;
                    } elseif($medidaSensor->timestamp > $nextTs){
                        break;
                    } else {
                        $insertKey++;
                    }
                }

                // Posição encontrada
                if($insertKey >= 0){
                    $newItem = new stdClass;
                    $newItem->tempo = $nextLabel;
                    $newItem->valor = null;
                    $newItem->timestamp = $nextTs;
                    array_splice($medidasSensores[$medidasSensorKey], $insertKey, 0, array($newItem));
                }

            }

            // Avaçar o ponteiro
            $leftmostTs = $nextTs;

        }

        return $medidasSensores;
    }

    /*
        Obter ultimas medições para dado sensor.  Caso um periodo não
        seja fornecido serão selecionados os ultimos 'qtd' valores.

        @param int - id do sensor
        @param dataInicio - string - Data inicial no formato 2016-01-04 00:00:00
        @param dataFinal - string - Datafinal no formato 2016-01-04 00:00:00
        $param qtd - int

        @return array de stdClass com propriedades:
            tempo
            valor
            timestamp
    */
    public static function obterValoresRecentesPorSensor($id, $dataInicio = null, $dataFim = null) {
        $query = DB::table('leitura')
        ->selectRaw('date_format(data_inclusao, "%d/%m/%Y - %H:%i:%s") as tempo, unix_timestamp(data_inclusao) as timestamp, valor as valor')
        ->where('id_sensorambienteequipamento', $id)
        ->orderBy('data_inclusao', 'desc');

        if($dataInicio)
            $query->where('data_inclusao', '>=', $dataInicio);
        if($dataFim)
            $query->where('data_inclusao', '<=', $dataFim);
        
        if($dataInicio == null && $dataFim == null)        
            $query->take(10);

        return $query->get();
    }   

    
}
