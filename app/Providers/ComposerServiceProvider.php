<?php namespace App\Providers;

use View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;
use App\User;
use App\Licenca;

class ComposerServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot(Request $request)
    {   

        /*
            Torna o $request accessivel em todas as views
        */
        View::share('request', $request);
        View::share('licenca_basica', Licenca::licencaBasica());
        View::share('licenca_grafico', Licenca::licencaGrafico());
        View::share('licenca_carta', Licenca::licencaCarta());
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}