<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Register root user caso não exista
        $users = User::where('username', 'root')->get();
        if(count($users) < 1) {
            $root= new User;
            $root->username = "root";
            $root->type = 0;
            $root->password = bcrypt('root');
            $root->save();
        }

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
