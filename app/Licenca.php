<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use stdClass;
use DB;

class Licenca extends Model
{
	protected $table = 'licenca';
	protected $primaryKey = 'id_licenca';

    /*
		Verifica se servidor tem licenca basica

		@return boolean

    */
    public static function licencaBasica() {
        $db = $licenca = DB::table('licenca')->get();

        if(count($db) <= 0 || $db[0]->flag_leitura == null) {
            return false;
        } else {
            return true;
        }

    }

    /*
        Verifica se servidor tem licenca de graficos

        @return boolean
        
    */
    public static function licencaGrafico() {
        $db = $licenca = DB::table('licenca')->get();
        
        if(count($db) <= 0 || $db[0]->flag_graficos == null) {
            return false;
        } else {
            return true;
        }
    }

    /*
        Verifica se servidor tem licenca de carta controle

        @return boolean
        
    */
    public static function licencaCarta() {
        $db = $licenca = DB::table('licenca')->get();
        
        if(count($db) <= 0 || $db[0]->flag_carta_controle == null) {
            return false;
        } else {
            return true;
        }
    }

    public static function atualizarCodigo($codigo) {
    	$db = self::first();

        if(!empty($db)){
            $db->desc_arquivo_licenca = $codigo;
            $db->save();
        } else {
            $db = new Licenca;
            $db->desc_arquivo_licenca = $codigo;
            $db->save();
        }
    }
    
}
