<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Http\Request;
use Illuminate\AuthController;


/*
	PERMITIR ACCESSO PARA ATUALIZAR LICENCA
*/
Route::group(['prefix' => 'config', 'middleware' => ['auth', 'config.access.control'] ], function () {

	// Licença
	Route::get('licenca', 'LicencaController@view');
	Route::post('licenca', 'LicencaController@atualizar');

});

/*
	LICENCA BASICA
	CONFIGURACOES
*/
Route::group(['prefix' => 'config', 'middleware' => ['auth', 'config.access.control', 'licenca.basica.control'] ], function () {

	// Ambientes
	Route::get('ambientes', 'AmbienteController@listView');
	Route::get('ambiente/adicionar', 'AmbienteController@addView');
	Route::get('ambiente/editar/{id}', 'AmbienteController@editView');
	Route::get('ambiente/remover/{id}', 'AmbienteController@remove');
	Route::post('ambiente', 'AmbienteController@update');

	// Equipamentos
	Route::get('equipamentos', 'EquipamentoController@listView');
	Route::get('equipamento/adicionar', 'EquipamentoController@addView');
	Route::get('equipamento/editar/{id}', 'EquipamentoController@editView');
	Route::get('equipamento/remover/{id}', 'EquipamentoController@remove');
	Route::post('equipamento', 'EquipamentoController@update');

	// Sensores
	Route::get('sensores', 'SensorController@listView');
	Route::get('sensor/adicionar', 'SensorController@addView');
	Route::get('sensor/editar/{id}', 'SensorController@editView');
	Route::get('sensor/remover/{id}', 'SensorController@remove');
	Route::post('sensor', 'SensorController@update');

	// Distribuição
	Route::get('distribuicao', 'DistribuicaoController@listView');
	Route::get('distribuicao/adicionar', 'DistribuicaoController@addView');
	Route::get('distribuicao/editar/{id}', 'DistribuicaoController@editView');
	Route::get('distribuicao/remover/{id}', 'DistribuicaoController@remove');
	Route::post('distribuicao', 'DistribuicaoController@update');

	// Usuários
	Route::get('usuarios', 'UsuarioController@listView');
	Route::get('usuario/adicionar', 'UsuarioController@addView');
	Route::get('usuario/remover/{id}', 'UsuarioController@remove');
	Route::get('usuario/editar/{id}', 'UsuarioController@editView');
	Route::post('usuario', 'UsuarioController@update');

	// Outliers
	Route::get('outliers', 'OutlierController@inicioView');

});

/*
	LICENCA BASICA
*/
Route::group(['middleware' => ['auth', 'licenca.basica.control']], function () {
	
	// home
	Route::get('/', 'HomeController@homeView');
	Route::get('/home', 'HomeController@homeView');

	// leituras
	Route::get('/leitura', 'LeituraController@leituraView');
	Route::post('/leitura/resultado', 'LeituraController@resultadoView');
	Route::get('/leitura/limpar_filtro', 'LeituraController@limpar_filtro');
	Route::post('/leitura/filtrar', 'LeituraController@filtrar');
});


/*
	LICENCA GRAFICO
*/
Route::group(['middleware' => ['auth', 'licenca.grafico.control']], function () {

	// graficos
	Route::get('/grafico', 'GraficoController@graficoView');
	Route::get('/grafico/sensor/{ids}', 'GraficoController@sensorData');
	Route::post('/grafico/resultado', 'GraficoController@resultadoView');
});


/*
	LICENCA CARTA CONTROLE
*/
Route::group(['middleware' => ['auth', 'config.access.control', 'licenca.carta.control']], function () {

	// carta controle
	// Route::get('/carta', 'CartaController@inicioView');
	// Temporariamente redireciona para graficos
	Route::get('/carta', 'GraficoController@graficoView');

});

// Fazer login
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::post('auth/login', 'Auth\AuthController@postLogin');

// Erro de licenca
Route::get('erro_licenca', 'ErroController@view');
