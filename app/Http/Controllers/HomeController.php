<?php

namespace App\Http\Controllers;

use App\User;
use App\Equipamento;
use App\Sensor;
use DB;
use App\Distribuicao;
use App\Leitura;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Redirect;
use Illuminate\Database\Eloquent\Collection;

class HomeController extends Controller
{
    public function homeView (Request $request) {
        $error = $request->session()->get('erro_inicio');

        // Obter 3 tipos de sensores
        $tipos_sensores = 
        	DB::table('sensor')
        	->select('id_sensor', 'desc_nome', 'desc_sigla')
        	->whereNull('sensor.deleted_at')
        	->take(12)
        	->get();

        $sensores = [];
        foreach($tipos_sensores as $tipo_sensor){
        	$sensores[] = [
        		'tipo' => $tipo_sensor->desc_nome,
        		'valores' => Leitura::obterValoresRecentesPorTipoSensor($tipo_sensor->id_sensor)
    		];
        }

        return view('panels.inicio', ['error' => $error, 'tipos_sensores' => $sensores]);
    }
}