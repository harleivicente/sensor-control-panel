<?php

namespace App\Http\Controllers;

use App\User;
use App\Ambiente;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Redirect;
use Auth;

class AmbienteController extends Controller
{
    /**
     * Lista todos os ambientes
     *
     * @return Response
     */
    public function listView()
    {
        $ambientes = Ambiente::all();
        return view('panels.config.ambiente.list', ['ambientes' => $ambientes]);
    }

    public function addView(Request $request) {
        $form_error = ['nome' => $request->session()->get('ambiente_erro_nome', false)];
        return view('panels.config.ambiente.add', ['form_error' => $form_error]);
    }

    public function editView(Request $request, $id) {
        $ambiente = Ambiente::find($id);
        $form_error = ['nome' => $request->session()->get('ambiente_erro_nome', false)];
        return view('panels.config.ambiente.edit', ['ambiente' => $ambiente, 'form_error' => $form_error]);
    }

    /**
    *   Adiciona/Atualiza ambiente
    */
    public function update(Request $request) {
        $nome = $request->input('ambienteNome');
        $id = $request->input('id_ambiente');

        // Verifica input
        if(empty($nome)) {
            $request->session()->flash('ambiente_erro_nome', true);
            return back();
        }

        if(!$id) {
            $item = new Ambiente;
        } else {
            $item = Ambiente::find($id);
        }
        $item->desc_nome = $nome;
        $item->save();

        return redirect()->action('AmbienteController@listView');
    } 

    public function remove($id) {
        $ambiente = Ambiente::find($id);
        $ambiente->delete();
        return redirect()->action('AmbienteController@listView');
    }  
}