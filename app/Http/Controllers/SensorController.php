<?php

namespace App\Http\Controllers;

use App\User;
use App\Equipamento;
use App\Sensor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Redirect;
use Illuminate\Database\Eloquent\Collection;

class SensorController extends Controller
{
    /**
     * Lista todos os sensores
     *
     * @return Response
     */
    public function listView()
    {
        $sensores = Sensor::all();
        return view('panels.config.sensor.list', ['sensores' => $sensores]);
    }

    public function addView(Request $request) {
        $form_error = ['nome' => $request->session()->get('sensor_erro_nome', false), 'sigla' => $request->session()->get('sensor_erro_sigla', false)];
        return view('panels.config.sensor.add', ['form_error' => $form_error]);
    }

    public function editView(Request $request, $id) {
        $sensor = Sensor::find($id);
        $form_error = ['nome' => $request->session()->get('sensor_erro_nome', false), 'sigla' => $request->session()->get('sensor_erro_sigla', false)];
        return view('panels.config.sensor.edit', ['sensor' => $sensor, 'form_error' => $form_error]);
    }

    /**
    *   Adiciona/Atualiza sensor
    */
    public function update(Request $request) {
        $nome = $request->input('sensorNome');
        $sigla = $request->input('sensorSigla');
        $id = $request->input('id_sensor');

        // Verifica input
        $erro = false;

        if(empty($nome)) {
            $request->session()->flash('sensor_erro_nome', true);
            $erro = true;
        }

        // verificar se sigla já é usada
        $usado = false;
        $items_existentes = Sensor::withTrashed()->where('desc_sigla', $sigla)->get();
        if(count($items_existentes) > 1){
            $usado = true;
        } elseif(count($items_existentes) === 1){
            if (!$id || $items_existentes[0]['id_sensor'] !== $id)
                $usado = true;
        }

        if(empty($sigla) || strlen($sigla) > 2 || $usado) {
            $request->session()->flash('sensor_erro_sigla', true);
            $erro = true;
        }

        if($erro){
            return back();
        }

        if(!$id) {
            $item = new Sensor;
        } else {
            $item = Sensor::find($id);
        }
        $item->desc_nome = $nome;
        $item->desc_sigla = $sigla;
        $item->save();

        return redirect()->action('SensorController@listView');
    } 

    public function remove($id) {
        $sensor = Sensor::find($id);
        $sensor->delete();
        return redirect()->action('SensorController@listView');
    }  
}