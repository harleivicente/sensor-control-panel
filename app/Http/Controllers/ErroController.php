<?php

namespace App\Http\Controllers;

use App\User;
use App\Equipamento;
use App\Sensor;
use App\Ambiente;
use App\Leitura;
use DB;
use App\Distribuicao;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Redirect;
use Illuminate\Database\Eloquent\Collection;
use stdClass;
use DateTime;
use DateInterval;

class ErroController extends Controller
{
    public function view (Request $request) {
        $error = $request->session()->get('erro_licenca_erro', null);

        $vars = [
            'error' => $error
        ];

        return view('panels.erro', $vars);
    }

}