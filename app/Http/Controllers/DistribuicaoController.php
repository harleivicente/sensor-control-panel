<?php

namespace App\Http\Controllers;

use App\User;
use App\Equipamento;
use App\Sensor;
use App\Ambiente;
use App\Distribuicao;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Redirect;
use Illuminate\Database\Eloquent\Collection;

class DistribuicaoController extends Controller
{
    /**
     * Lista todos as configurações de distribuição
     *
     * @return Response
     */
    public function listView()
    {   

        /*
            Formato da tabela criada

            id | ambiente_nome | equipamento_nome | equipamento_codigo | sensor_nome | sensor_sigla
        */
        $configuracoes = Distribuicao::getBasicQuery()->get();


        return view('panels.config.distribuicao.list', ['configuracoes' => $configuracoes]);
    }

    public function addView(Request $request) {
        $ambientes = Ambiente::all();
        $equipamentos = Equipamento::all();
        $sensores = Sensor::all();
        $generic = $request->session()->get('distribuicao_erro', false);

        $vars = [
            'form_error' => ['generic' => $generic],
            'ambientes' => $ambientes,
            'equipamentos' => $equipamentos,
            'sensores' => $sensores
        ];
        return view('panels.config.distribuicao.add', $vars);
    }

    public function editView(Request $request, $id) {
        $ambientes = Ambiente::all();
        $equipamentos = Equipamento::all();
        $sensores = Sensor::all();
        $distribuicao = Distribuicao::find($id);
        $generic = $request->session()->get('distribuicao_erro', false);

        $vars = [
            'form_error' => ['generic' => $generic],
            'ambientes' => $ambientes,
            'equipamentos' => $equipamentos,
            'sensores' => $sensores,
            'distribuicao' => $distribuicao
        ];
        return view('panels.config.distribuicao.edit', $vars);
    }

    /**
    *   Adiciona/Atualiza configuração de distribuição
    */
    public function update(Request $request) {
        $ambienteId = $request->input('distAmbiente');
        $equipamentoId = $request->input('distEquipamento');
        $sensorId = $request->input('distSensor');
        $id = $request->input('distId');

        // Verifica se ambiente, equipamento e sensores indicados existem no DB.
        $ambiente = Ambiente::find($ambienteId);
        $equipamento = Equipamento::find($equipamentoId);
        $sensor = Sensor::find($sensorId);

        $erro = false;
        $erro = !$ambiente || !$equipamento || !$sensor;

        if($erro){
            $request->session()->flash('distribuicao_erro', true);
            return back();
        }

        // Cria/atualiza dados
        $item = Distribuicao::findOrNew($id);
        $item->id_ambiente = $ambienteId;
        $item->id_equipamento = $equipamentoId;
        $item->id_codigosensor = 1; // O que esse atributo representa ?
        $item->id_sensor = $sensorId;
        $item->save();

        return redirect()->action('DistribuicaoController@listView');
    } 

    public function remove($id) {
        $distribuicao = Distribuicao::find($id);
        $distribuicao->delete();
        return redirect()->action('DistribuicaoController@listView');
    }  
}
