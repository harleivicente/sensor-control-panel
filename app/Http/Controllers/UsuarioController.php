<?php

namespace App\Http\Controllers;

use App\User;
use App\Equipamento;
use App\Sensor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Redirect;
use Illuminate\Database\Eloquent\Collection;

class UsuarioController extends Controller
{
    /**
     * Lista todos os usuários
     *
     * @return Response
     */
    public function listView(Request $request)
    {
        // Listar usuários root se usuário logado for root
        if($request->user()->isRoot()){
            $permissao = 0;
        } else {
            $permissao = 1;
        }
        $usuarios = User::where('type', '>=', $permissao)->get();
        $error= $request->session()->get('erro_usuario_list');
        return view('panels.config.usuario.list', ['usuarios' => $usuarios, 'error' => $error]);
    }


    public function addView(Request $request) {
        $vars = [
            'form_error' => [
                'username' => $request->session()->get('usuario_erro_form_username', null),
                'password' => $request->session()->get('usuario_erro_form_password', null)
            ],
            'types' => [
                ['codigo' => 1, 'texto' => 'Administrador'],
                ['codigo' => 2, 'texto' => 'Regular']
            ]
        ];

        // Usuários root podem criar outros usuários root
        if($request->user()->isRoot())
            $vars['types'][] = ['codigo' => 0, 'texto' => 'Root'];

        return view('panels.config.usuario.add', $vars);
    }


    public function editView(Request $request, $id) {
        $usuario = User::find($id);

        // Verificar se usuário existe

        // Para editar usuários do tipo root deve estar logado como root
        $editedUser = User::find($id);
        $loggedUser = $request->user();
        if($editedUser->isRoot() && !$loggedUser->isRoot()) {
            $request->session()->flash('erro_usuario_list', "Você não tem permissão para editar esse tipo de usuário.");
            return redirect()->action('UsuarioController@listView');
        }

        $vars = [
            'form_error' => [
                'username' => $request->session()->get('usuario_erro_form_username', null),
                'password' => $request->session()->get('usuario_erro_form_password', null)
            ],
            'types' => [
                ['codigo' => 1, 'texto' => 'Administrador'],
                ['codigo' => 2, 'texto' => 'Regular']
            ],
            'user' => $usuario
        ];

        // Usuários root podem criar outros usuários root
        if($request->user()->isRoot())
            $vars['types'][] = ['codigo' => 0, 'texto' => 'Root'];

        return view('panels.config.usuario.edit', $vars);
    }

    /*
        Creates / edits user

        Mensagens de erros de formulário na criação de usuários na variáveis
        de sessão:
            usuario_erro_form_username, usuario_erro_form_password, erro_usuario_list
    */
    public function update(Request $request) {
        $username = $request->input('username');
        $password = $request->input('password');
        $type = $request->input('type');
        $id = $request->input('userId');


        // Para editar usuários do tipo root ou tornar algum em root deve estar logado como root
        $editedUser = User::find($id);
        $loggedUser = $request->user();
        if((($id && $editedUser->isRoot()) || $type == "0") && !$loggedUser->isRoot()) {
            $request->session()->flash('erro_usuario_list', "Você não tem permissão para editar esse tipo de usuário.");
            return redirect()->action('UsuarioController@listView');
        }

        // Verifica input
        $erro = false;
        if(empty($username)) {
            $request->session()->flash('usuario_erro_form_username', "Nome de usuário inválido.");
            $erro = true;
        }

        // verificar se username já é usado
        $usado = false;
        $usuarios_existentes = User::where('username', $username)->get();
        if(count($usuarios_existentes) > 1){
            $usado = true;
        } elseif(count($usuarios_existentes) === 1){
            if (!$id || $usuarios_existentes[0]['id_seglogin'] !== $id)
                $usado = true;
        }

        if($usado) {
            $request->session()->flash('usuario_erro_form_username', "Usuário já usado.");
            $erro = true;
        }

        if(!empty($password) && (strlen($password) < 6 || strlen($password) > 16)) {
            $request->session()->flash('usuario_erro_form_password', "Password deve ter entre 6 e 16 letras.");
            $erro = true;
        }
        if($erro){
            return back();
        }

        // Cria/atualiza usuário
        if(!$id) {
            $item = new User;
        } else {
            $item = User::find($id);
        }
        $item->username = $username;


        // Caso usuário tenha preenchido input de password, mudar a senha
        if(!empty($password))
            $item->password = bcrypt($password);

        $item->type = $type;
        $item->save();

        return redirect()->action('UsuarioController@listView');
    }


    public function remove(Request $request, $id) {
        $usuario = User::find($id);

        // Para remover usuários do tipo root deve estar logado como root
        $editedUser = User::find($id);
        $loggedUser = $request->user();
        if($editedUser->isRoot() && !$loggedUser->isRoot()) {
            $request->session()->flash('erro_usuario_list', "Você não tem permissão para editar esse tipo de usuário.");
            return redirect()->action('UsuarioController@listView');
        }

        $usuario->delete();
        return redirect()->action('UsuarioController@listView');
    }

    
}