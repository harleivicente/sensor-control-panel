<?php

namespace App\Http\Controllers;

use App\User;
use App\Equipamento;
use App\Sensor;
use App\Ambiente;
use App\Leitura;
use DB;
use App\Distribuicao;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Redirect;
use Illuminate\Database\Eloquent\Collection;
use stdClass;
use DateTime;
use DateInterval;

class GraficoController extends Controller
{
    public function graficoView (Request $request) {
        $ambientes = Ambiente::all();
        $equipamentos = Equipamento::all();
        $sensores = Sensor::all();

        $distribuicoes = Distribuicao::obterDistribuicoesFiltradas($request->session());
        $error = $request->session()->get('grafico_erro', null);

        $vars = [
            'ambientes' => $ambientes,
            'equipamentos' => $equipamentos,
            'sensores' => $sensores,
            'ambienteId' => $request->session()->get('sensor_filtro_ambiente_id', null),
            'equipamentoId' => $request->session()->get('sensor_filtro_equipamento_id', null),
            'sensorId' => $request->session()->get('sensor_filtro_sensor_id', null),
            'distribuicoes' => $distribuicoes,
            'error' => $error
        ];
        return view('panels.grafico.selecao', $vars);
    }

    /*
        Fornece interface JSON para obter ultimas medidas
        de sensores.

        @param array de ids dos sensores
        @return - JSON - array de:
            [
                stdClass
                    - tempo <string no formato : 2016-01-04 00:00:00>
                    - valor <int>
                    - timestamp <unix timestamp>
                ,
                ....
            ]
    */
    public function sensorData(Request $request, $sensorIdsJSON) {
        $sensorIds = json_decode($sensorIdsJSON);
        $results = [];

        foreach($sensorIds as $sensorId){
            $results[] = array_reverse(Leitura::obterValoresRecentesPorSensor($sensorId));
        }

        // Alinhar valores
        $results = Leitura::alinharMedidasSensores($results);
        return json_encode($results);
    }

    public function resultadoView(Request $request) {
        $distribuicoesId = $request->input('distribuicoesId', []);
        $tempoReal = $request->input('tempoReal', false);
        $dataInicio = $request->input('dataInicio', null);
        $dataFim = $request->input('dataFim', null);

        // Verifica se sensores foram selecionados
        if(count($distribuicoesId) < 1) {
            $request->session()->flash('grafico_erro', "Nenhum sensor selecionado.");
            return back();
        }

        // Coletar medidas para cada um dos sensores selecionados
        $medidasSensoresStd = [];
        foreach($distribuicoesId as $distribuicaoId) {
            $dadosDB = Leitura::obterValoresRecentesPorSensor(
                    $distribuicaoId,
                    $dataInicio,
                    $dataFim
                );

            // Inversão para que primeiro item no array seja a medição mais antiga
            $medidasSensoresStd[] = array_reverse($dadosDB);
        }

        // Alinhamento dos dados
        $medidasSensoresStd = Leitura::alinharMedidasSensores($medidasSensoresStd);

        /*
            Construir array no formato utilizado pela view:
                - array de items no formato:
                [
                    'tipo' - string (i.e Temperatura)
                    'medicoes' - Array de:
                        ['codigo' => 2, values =>[<int>, ...]]
                ]
        */
        $dados = [];

        /*
            Contabilizar os diferentes tipos de sensores entre os 
            selecionados. (i.e Temperatura, Umidade, ....)

            Array - int
        */
        $tipoSensorIds = [];

        /*
            Os nomes para o eixo horizontal dos gráficos

            Array - string
        */
        $labels = [];

        foreach ($distribuicoesId as $distribuicaoId) {
            $distribuicao = Distribuicao::find($distribuicaoId);
                if($distribuicao && !in_array($distribuicao->id_sensor, $tipoSensorIds))
                    array_push($tipoSensorIds, $distribuicao->id_sensor);
        }

        foreach($tipoSensorIds as $tipoSensorId){
            $sensor = Sensor::find($tipoSensorId);
            if($sensor){
                $dados[] = [
                    'tipo' => $sensor->desc_nome,
                    'tipo_id' => $sensor->id_sensor,
                    'medicoes' => []
                ];
            }
        }

        foreach($distribuicoesId as $key => $distribuicaoId){
            $distribuicao = Distribuicao::find($distribuicaoId);
            if($distribuicao){
                $medicoesInt = [];
                $medidasSensorStdArray = $medidasSensoresStd[$key];

                // Criar array de int com medições para cada sensor
                foreach($medidasSensorStdArray as $medidaStd) {
                    array_push($medicoesInt, $medidaStd->valor);

                    if($key === 0)
                        array_push($labels, $medidaStd->tempo);
                }

                //  Encontar posição para inserir medições em '$dados'
                foreach($dados as $key2 => $dado) {
                    if($distribuicao->id_sensor == $dado['tipo_id']){
                        $dados[$key2]['medicoes'][] = [
                            'codigo' => $distribuicao->id_sensorambienteequipamento,
                            'values' => $medicoesInt
                        ];
                    }
                }
            }
        }

        return view('panels.grafico.resultado', ['dados' => $dados, 'labels' => $labels, 'tempoReal' => $tempoReal]);
    }

}