<?php

namespace App\Http\Controllers;

use App\User;
use App\Equipamento;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Redirect;
use Illuminate\Database\Eloquent\Collection;

class EquipamentoController extends Controller
{
    /**
     * Lista todos os equipamentos
     *
     * @return Response
     */
    public function listView()
    {
        $equipamentos = Equipamento::all();
        return view('panels.config.equipamento.list', ['equipamentos' => $equipamentos]);
    }

    public function addView(Request $request) {
        $form_error = ['nome' => $request->session()->get('equipamento_erro_nome', false), 'codigo' => $request->session()->get('equipamento_erro_codigo', false)];
        return view('panels.config.equipamento.add', ['form_error' => $form_error]);
    }

    public function editView(Request $request, $id) {
        $equipamento = Equipamento::find($id);
        $form_error = ['nome' => $request->session()->get('equipamento_erro_nome', false), 'codigo' => $request->session()->get('equipamento_erro_codigo', false)];
        return view('panels.config.equipamento.edit', ['equipamento' => $equipamento, 'form_error' => $form_error]);
    }

    /**
    *   Adiciona/Atualiza Equipamento
    */
    public function update(Request $request) {
        $nome = $request->input('equipamentoNome');
        $codigo = $request->input('equipamentoCodigo');
        $id = $request->input('id_equipamento');

        // Verifica input
        $erro = false;

        if(empty($nome)) {
            $request->session()->flash('equipamento_erro_nome', true);
            $erro = true;
        }

        // verificar se codigo já é usado
        $usado = false;
        $equipamentos_existentes = Equipamento::withTrashed()->where('desc_codigo', $codigo)->get();
        if(count($equipamentos_existentes) > 1){
            $usado = true;
        } elseif(count($equipamentos_existentes) === 1){
            if (!$id || $equipamentos_existentes[0]['id_equipamento'] !== $id)
                $usado = true;
        }

        if(empty($codigo) || strlen($codigo) > 8 || $usado) {
            $request->session()->flash('equipamento_erro_codigo', true);
            $erro = true;
        }

        if($erro){
            return back();
        }

        if(!$id) {
            $item = new Equipamento;
        } else {
            $item = Equipamento::find($id);
        }
        $item->desc_nome = $nome;
        $item->desc_codigo = $codigo;
        $item->save();

        return redirect()->action('EquipamentoController@listView');
    } 

    public function remove($id) {
        $equipamento = Equipamento::find($id);
        $equipamento->delete();
        return redirect()->action('EquipamentoController@listView');
    }  
}