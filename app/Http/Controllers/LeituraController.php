<?php

namespace App\Http\Controllers;

use App\User;
use App\Equipamento;
use App\Sensor;
use App\Ambiente;
use App\Leitura;
use DB;
use App\Distribuicao;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Redirect;
use Illuminate\Database\Eloquent\Collection;
use stdClass;

class LeituraController extends Controller
{
    public function leituraView (Request $request) {
        $ambientes = Ambiente::all();
        $equipamentos = Equipamento::all();
        $sensores = Sensor::all();

        $distribuicoes = Distribuicao::obterDistribuicoesFiltradas($request->session());
        $error = $request->session()->get('leitura_erro', null);

        $vars = [
            'ambientes' => $ambientes,
            'equipamentos' => $equipamentos,
            'sensores' => $sensores,
            'ambienteId' => $request->session()->get('sensor_filtro_ambiente_id', null),
            'equipamentoId' => $request->session()->get('sensor_filtro_equipamento_id', null),
            'sensorId' => $request->session()->get('sensor_filtro_sensor_id', null),
            'distribuicoes' => $distribuicoes,
            'error' => $error
        ];
        return view('panels.leitura.selecao', $vars);
    }

    
    public function resultadoView(Request $request) {
        $filterAmbiente = $request->input('distribuicoesId', []);
        $dados = [];

        if(count($filterAmbiente) < 1) {
            $request->session()->flash('leitura_erro', "Nenhum sensor selecionado.");
            return back();
        }

        // String no formato "2016-01-04 00:00:00"
        $dataInicio = $request->input('dataInicio', null);
        $dataFim = $request->input('dataFim', null);

        foreach($filterAmbiente as $filter) {
            $dados[] = [
                'distribuicao' => Distribuicao::obterDistribuicao($filter),
                'medicoes' => Leitura::obterValoresRecentesPorSensor($filter, $dataInicio, $dataFim)
            ];
        }

        // Dados devem ser alinhados
        
        return view('panels.leitura.resultado', ['dados' => $dados]);
    }

    /*
        Aplica configurações de filtros nas páginas /leitura e /graficos

    */
    public function filtrar (Request $request) {
        $filterAmbiente = $request->input('filterAmbiente');
        $filterEquipamento = $request->input('filterEquipamento');
        $filterSensor = $request->input('filterSensor');

        if(Ambiente::find($filterAmbiente))
            $request->session()->set('sensor_filtro_ambiente_id', $filterAmbiente);
        if(Equipamento::find($filterEquipamento))
            $request->session()->set('sensor_filtro_equipamento_id', $filterEquipamento);
        if(Sensor::find($filterSensor))
            $request->session()->set('sensor_filtro_sensor_id', $filterSensor);
        return back();
    }

    /*
        Limpa os filtros
    */
    public function limpar_filtro(Request $request) {
        $request->session()->set('sensor_filtro_ambiente_id', null);
        $request->session()->set('sensor_filtro_equipamento_id', null);
        $request->session()->set('sensor_filtro_sensor_id', null);
        return back();
    }
}