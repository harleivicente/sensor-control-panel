<?php

namespace App\Http\Controllers;

use App\User;
use App\Licenca;
use DB;
use App\Distribuicao;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Redirect;
use Illuminate\Database\Eloquent\Collection;


use stdClass;
use DateTime;
use DateInterval;

class LicencaController extends Controller
{
    public function view (Request $request) {
        $error = $request->session()->get('erro_licenca_codigo', null);
        $ok = $request->session()->get('ok_licenca_codigo', null);

        $vars = [
            'error' => $error,
            'ok' => $ok
        ];

        return view('panels.config.licenca', $vars);
    }

    public function atualizar(Request $request) {
        $licenca = $request->input('licenca');
        
        // Verifica input
        if(strlen($licenca) > 20) {
            $request->session()->flash('erro_licenca_codigo', true);
        } else {
	        Licenca::atualizarCodigo($licenca);
            $request->session()->flash('ok_licenca_codigo', true);
        }

        return back();
    }

}