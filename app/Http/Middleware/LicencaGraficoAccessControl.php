<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Licenca;
use URL;

class LicencaGraficoAccessControl
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * Caso o usuário logado não seja root ou administrador a requisição
     * é redirecionada.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $licenca = Licenca::licencaGrafico();

        if($licenca) {
            return $next($request);
        } else {
            $request->session()->flash('erro_licenca_erro', "Sua licença está vencida ou não inclui esse módulo.");
            return redirect(URL::to('/erro_licenca'));
        }
    }
}
