<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Equipamento extends Model
{
	use SoftDeletes;

	protected $table = 'equipamento';
	protected $primaryKey = 'id_equipamento';
    
}
