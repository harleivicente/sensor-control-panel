<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


/*

    Existem 3 tipos de usuários:

    com atributo type 0: usuário root, tem accesso total ao sistema

    type 1: usuário administrador, tem accesso total no escopo
    do cliente (criar/editar/deletar ambiente, equipamento, sensor, distribuição, usuários tipo 1/2)

    type 2: usuário regular, apenas visualiza dados dos sensores

*/
class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'seg_login';
    protected $primaryKey = 'id_seglogin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    public function isRoot() {
        return (isset($this->type) && $this->type == "0");
    }

    public function isAdmin() {
        return (isset($this->type) && $this->type == "1");
    }

    public function canConfig() {
        return $this->isRoot() || $this->isAdmin();
    }
}
