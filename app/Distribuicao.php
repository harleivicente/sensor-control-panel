<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;


class Distribuicao extends Model
{
	use SoftDeletes;

	protected $table = 'sensor_ambiente_equipamento';
	protected $primaryKey = 'id_sensorambienteequipamento';


	/*
		Obtem as distribuições cadastradas (não deletadas). Filtros de ambiente, equipamento e tipo de sensor
		são aplicados. Filtros nas sessão (sensor_filtro_ambiente_id, sensor_filtro_equipamento_id, sensor_filtro_sensor_id)
	*/
	public static function obterDistribuicoesFiltradas($session) {
		$ambienteId = $session->get('sensor_filtro_ambiente_id', null);
        $equipamentoId = $session->get('sensor_filtro_equipamento_id', null);
        $sensorId = $session->get('sensor_filtro_sensor_id', null);

    	/*
            Formato da tabela criada

            id | ambiente_nome | equipamento_nome | equipamento_codigo | sensor_nome | sensor_sigla
        */
        $query = self::getBasicQuery();

        if($ambienteId)
        	$query->where('sensor_ambiente_equipamento.id_ambiente', $ambienteId);
        if($equipamentoId)
        	$query->where('sensor_ambiente_equipamento.id_equipamento', $equipamentoId);
        if($sensorId)
        	$query->where('sensor_ambiente_equipamento.id_sensor', $sensorId);

        return $query->get();
	}

    /*
        Obtem distribuição com os seguintes campos (stdClass):
            id | ambiente_nome | equipamento_nome | equipamento_codigo | sensor_nome | sensor_sigla
    */
    public static function obterDistribuicao($id) {
        $result = self::getBasicQuery()->where('sensor_ambiente_equipamento.id_sensorambienteequipamento', $id)->get();
        return empty($result) ? null : $result[0];
    }

	/*
		Obtem a query SQL basica para uma tabela com os seguintes campos:
            id | ambiente_nome | equipamento_nome | equipamento_codigo | sensor_nome | sensor_sigla
	*/
	public static function getBasicQuery() {
        $query = DB::table('sensor_ambiente_equipamento')
            ->leftJoin('ambiente', function($join){
                $join->on('sensor_ambiente_equipamento.id_ambiente', '=', 'ambiente.id_ambiente')
                ->whereNull('ambiente.deleted_at');
            })
            ->leftJoin('equipamento', function($join){
                $join->on('sensor_ambiente_equipamento.id_equipamento', '=', 'equipamento.id_equipamento')
                ->whereNull('equipamento.deleted_at');
            })
            ->leftJoin('sensor', function($join){
                $join->on('sensor_ambiente_equipamento.id_sensor', '=', 'sensor.id_sensor')
                ->whereNull('sensor.deleted_at');
            })
            ->selectRaw('
                sensor_ambiente_equipamento.id_sensorambienteequipamento as id,
                ambiente.desc_nome as ambiente_nome,
                equipamento.desc_nome as equipamento_nome,
                equipamento.desc_codigo as equipamento_codigo,
                sensor.desc_nome as sensor_nome,
                sensor.desc_sigla as sensor_sigla'
            )
            ->whereNull("sensor_ambiente_equipamento.deleted_at");
        return $query;
	}
    
}
