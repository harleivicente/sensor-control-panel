<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sensor extends Model
{
	use SoftDeletes;

	protected $table = 'sensor';
	protected $primaryKey = 'id_sensor';
    
}
