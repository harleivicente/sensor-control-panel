-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: smca
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ambiente`
--

DROP TABLE IF EXISTS `ambiente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ambiente` (
  `data_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_exclusao` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `desc_nome` varchar(60) NOT NULL,
  `id_ambiente` int(11) NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_ambiente`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ambiente`
--

LOCK TABLES `ambiente` WRITE;
/*!40000 ALTER TABLE `ambiente` DISABLE KEYS */;
INSERT INTO `ambiente` VALUES ('2016-06-25 19:48:30','0000-00-00 00:00:00','Lab A',1,NULL,'0000-00-00 00:00:00','2016-06-25 22:48:30'),('2016-02-01 21:52:21','0000-00-00 00:00:00','Laboratorio B',24,'2016-02-01 23:52:21','2016-02-01 02:21:58','2016-02-01 23:52:21'),('2016-06-25 19:48:35','0000-00-00 00:00:00','Lab B',25,NULL,'2016-02-03 03:17:20','2016-06-25 22:48:35'),('2016-06-25 19:48:39','0000-00-00 00:00:00','Lab C',26,NULL,'2016-06-25 22:48:39','2016-06-25 22:48:39');
/*!40000 ALTER TABLE `ambiente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aux_dispositivo`
--

DROP TABLE IF EXISTS `aux_dispositivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aux_dispositivo` (
  `desc_dispositivo` varchar(30) NOT NULL,
  `flag_emuso` bit(1) NOT NULL DEFAULT b'0',
  UNIQUE KEY `desc_dispositivo` (`desc_dispositivo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aux_dispositivo`
--

LOCK TABLES `aux_dispositivo` WRITE;
/*!40000 ALTER TABLE `aux_dispositivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `aux_dispositivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buffer_serial`
--

DROP TABLE IF EXISTS `buffer_serial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buffer_serial` (
  `data_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `desc_protocolo` varchar(40) NOT NULL,
  `flag_sucesso` bit(1) NOT NULL DEFAULT b'0',
  `flag_pendente` bit(1) NOT NULL DEFAULT b'1',
  `desc_log` varchar(100) NOT NULL,
  `id_bufferserial` int(11) NOT NULL AUTO_INCREMENT,
  `data_resposta` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_bufferserial`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buffer_serial`
--

LOCK TABLES `buffer_serial` WRITE;
/*!40000 ALTER TABLE `buffer_serial` DISABLE KEYS */;
INSERT INTO `buffer_serial` VALUES ('2015-07-15 19:39:49','!endereco:','','\0','LABA;TT;089',1,'2015-07-13 20:24:24'),('2015-07-15 19:40:40','!endereco:','','\0','',2,'2015-07-15 19:40:40'),('2015-07-15 19:49:53','!endereco:','','\0','',3,'2015-07-15 19:49:53'),('2015-07-15 19:52:07','!endereco:','','\0','',4,'2015-07-15 19:52:07'),('2015-07-15 19:53:42','!endereco:011','','\0','',5,'2015-07-15 19:53:42'),('2015-07-15 19:55:35','!endereco:099','','\0','',6,'2015-07-15 19:55:35'),('2015-07-15 19:59:31','!endereco:011','','\0','',7,'2015-07-15 19:59:31'),('2015-07-15 19:59:55','!endereco:031','','\0','',8,'2015-07-15 19:59:55'),('2015-07-15 20:23:43','!endereco:','','\0','',9,'2015-07-15 20:23:43'),('2015-07-15 20:23:43','!endereco:','','\0','',10,'2015-07-15 20:23:43'),('2015-07-15 20:33:04','!endereco:','','\0','',11,'2015-07-15 20:33:04'),('2015-07-15 20:42:24','!endereco:','','\0','',12,'2015-07-15 20:42:24'),('2015-07-15 20:44:05','!endereco:','','\0','',13,'2015-07-15 20:44:05'),('2015-07-16 15:35:21','!endereco:','\0','\0','',14,'2015-07-16 15:35:21'),('2015-07-16 15:46:12','!endereco','\0','\0','',15,'2015-07-16 15:46:12'),('2015-07-16 15:48:32','!endereco:','\0','\0','',16,'2015-07-16 15:48:32'),('2015-07-16 15:53:39','!endereco','\0','\0','',17,'2015-07-16 15:53:39'),('2015-07-16 17:25:22','!endereco:324','\0','\0','',18,'2015-07-16 17:25:22');
/*!40000 ALTER TABLE `buffer_serial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `codigo_sensor`
--

DROP TABLE IF EXISTS `codigo_sensor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codigo_sensor` (
  `id_codigosensor` int(11) NOT NULL AUTO_INCREMENT,
  `desc_codigo` varchar(3) NOT NULL,
  `data_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_exclusao` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `flag_emuso` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id_codigosensor`),
  UNIQUE KEY `desc_codigo` (`desc_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `codigo_sensor`
--

LOCK TABLES `codigo_sensor` WRITE;
/*!40000 ALTER TABLE `codigo_sensor` DISABLE KEYS */;
INSERT INTO `codigo_sensor` VALUES (1,'011','2015-07-15 19:54:39','0000-00-00 00:00:00',''),(2,'002','2015-07-15 19:57:07','0000-00-00 00:00:00','\0'),(3,'089','2015-07-08 20:02:13','0000-00-00 00:00:00',''),(4,'081','2015-07-13 20:29:14','0000-00-00 00:00:00',''),(5,'031','2015-07-15 19:20:37','0000-00-00 00:00:00','');
/*!40000 ALTER TABLE `codigo_sensor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipamento`
--

DROP TABLE IF EXISTS `equipamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipamento` (
  `id_equipamento` int(11) NOT NULL AUTO_INCREMENT,
  `data_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_exclusao` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `desc_nome` varchar(40) NOT NULL,
  `desc_codigo` varchar(8) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_equipamento`),
  UNIQUE KEY `desc_nome` (`desc_nome`),
  UNIQUE KEY `desc_codigo` (`desc_codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipamento`
--

LOCK TABLES `equipamento` WRITE;
/*!40000 ALTER TABLE `equipamento` DISABLE KEYS */;
INSERT INTO `equipamento` VALUES (1,'2016-02-01 02:35:01','0000-00-00 00:00:00','Geladeira','GL',NULL,'0000-00-00 00:00:00','2016-02-01 04:30:36'),(24,'2016-02-06 02:28:50','0000-00-00 00:00:00','Tanque','TQ',NULL,'2016-02-01 02:28:58','2016-02-06 04:28:50'),(25,'2016-02-03 01:18:17','0000-00-00 00:00:00','Gaveta','GV',NULL,'2016-02-03 03:18:17','2016-02-03 03:18:17');
/*!40000 ALTER TABLE `equipamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leitura`
--

DROP TABLE IF EXISTS `leitura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leitura` (
  `id_leitura` int(11) NOT NULL AUTO_INCREMENT,
  `data_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_exclusao` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_sensorambienteequipamento` int(11) NOT NULL,
  `valor` double NOT NULL,
  PRIMARY KEY (`id_leitura`),
  KEY `leitura_id_sensorambienteequipamento_idx` (`id_sensorambienteequipamento`),
  CONSTRAINT `leitura_ibfk_1` FOREIGN KEY (`id_sensorambienteequipamento`) REFERENCES `sensor_ambiente_equipamento` (`id_sensorambienteequipamento`)
) ENGINE=InnoDB AUTO_INCREMENT=18624 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leitura`
--

LOCK TABLES `leitura` WRITE;
/*!40000 ALTER TABLE `leitura` DISABLE KEYS */;
INSERT INTO `leitura` VALUES (18490,'2016-01-02 02:00:00','0000-00-00 00:00:00',11,15),(18491,'2016-01-01 02:00:00','0000-00-00 00:00:00',12,10),(18493,'2016-01-03 02:00:00','0000-00-00 00:00:00',12,30),(18496,'2016-01-02 02:00:00','0000-00-00 00:00:00',12,56),(18499,'2016-01-01 02:00:00','0000-00-00 00:00:00',11,46),(18544,'2016-01-03 02:00:00','0000-00-00 00:00:00',11,5),(18604,'2016-02-09 20:12:47','0000-00-00 00:00:00',11,12),(18605,'2016-02-09 20:12:47','0000-00-00 00:00:00',12,32),(18606,'2016-02-09 20:13:06','0000-00-00 00:00:00',11,8),(18607,'2016-02-09 20:13:06','0000-00-00 00:00:00',12,35),(18608,'2016-02-09 20:13:25','0000-00-00 00:00:00',11,12),(18609,'2016-02-09 20:13:25','0000-00-00 00:00:00',12,29),(18610,'2016-02-09 20:13:40','0000-00-00 00:00:00',11,10),(18611,'2016-02-09 20:13:40','0000-00-00 00:00:00',12,40),(18612,'2016-02-09 20:14:02','0000-00-00 00:00:00',11,0),(18613,'2016-02-09 20:14:02','0000-00-00 00:00:00',12,50),(18614,'2016-02-09 20:14:17','0000-00-00 00:00:00',11,40),(18615,'2016-02-09 20:14:17','0000-00-00 00:00:00',12,0),(18616,'2016-02-09 20:14:31','0000-00-00 00:00:00',11,0),(18617,'2016-02-09 20:14:31','0000-00-00 00:00:00',12,40),(18618,'2016-02-09 20:14:52','0000-00-00 00:00:00',11,10),(18619,'2016-02-09 20:14:52','0000-00-00 00:00:00',12,30),(18620,'2016-03-11 19:16:33','0000-00-00 00:00:00',11,45),(18621,'2016-03-11 19:16:33','0000-00-00 00:00:00',12,21),(18622,'2016-03-11 19:18:03','0000-00-00 00:00:00',11,12),(18623,'2016-03-11 19:18:03','0000-00-00 00:00:00',12,21);
/*!40000 ALTER TABLE `leitura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `licenca`
--

DROP TABLE IF EXISTS `licenca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licenca` (
  `id_licenca` int(11) NOT NULL AUTO_INCREMENT,
  `data_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_inicio` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data_expiracao` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `desc_arquivo_licenca` varchar(40) DEFAULT NULL,
  `flag_leitura` bit(1) DEFAULT NULL,
  `flag_graficos` bit(1) DEFAULT NULL,
  `flag_carta_controle` bit(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_licenca`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `licenca`
--

LOCK TABLES `licenca` WRITE;
/*!40000 ALTER TABLE `licenca` DISABLE KEYS */;
INSERT INTO `licenca` VALUES (2,'2016-02-24 03:25:06','0000-00-00 00:00:00','0000-00-00 00:00:00','abc3','','','','2016-02-24 02:54:51','2016-02-24 02:56:03',NULL);
/*!40000 ALTER TABLE `licenca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outlier`
--

DROP TABLE IF EXISTS `outlier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outlier` (
  `id_outlier` int(11) NOT NULL AUTO_INCREMENT,
  `id_equipamento` int(11) NOT NULL,
  `id_sensor` int(11) NOT NULL,
  `data_vigencia` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_inclusao` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data_exclusao` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `minimo` float DEFAULT NULL,
  `maximo` float DEFAULT NULL,
  PRIMARY KEY (`id_outlier`),
  KEY `fk_equipamento` (`id_equipamento`),
  KEY `fk_sensor` (`id_sensor`),
  CONSTRAINT `fk_equipamento` FOREIGN KEY (`id_equipamento`) REFERENCES `equipamento` (`id_equipamento`),
  CONSTRAINT `fk_sensor` FOREIGN KEY (`id_sensor`) REFERENCES `sensor` (`id_sensor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outlier`
--

LOCK TABLES `outlier` WRITE;
/*!40000 ALTER TABLE `outlier` DISABLE KEYS */;
/*!40000 ALTER TABLE `outlier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seg_login`
--

DROP TABLE IF EXISTS `seg_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seg_login` (
  `id_seglogin` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(60) NOT NULL,
  `data_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_exclusao` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(100) DEFAULT NULL,
  `type` int(10) NOT NULL DEFAULT '2',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_seglogin`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seg_login`
--

LOCK TABLES `seg_login` WRITE;
/*!40000 ALTER TABLE `seg_login` DISABLE KEYS */;
INSERT INTO `seg_login` VALUES (30,'admin','$2y$10$qMD0FM/xgFXIvjxsLkrjye7D1XXkxEvo2oh2BNDYJxrIrzYXFkX9K','2016-02-24 03:24:29','0000-00-00 00:00:00','TQLasSfZiIUWKfjTdBnCLLTpVpqZeAOULuIc71dIzn4GiMMNUnyxcBfWLf1B',1,'2016-02-02 23:53:13','2016-02-24 03:24:29',NULL),(31,'regular','$2y$10$5xDMKrgDIgE0xOfXmtBDvOnX2lZYad5UHjeWHVNY7kwCK5PXheHa6','2016-03-11 19:28:39','0000-00-00 00:00:00','Fp4M9lEKtO84UzvdWIefCmBaZ3r4BwvpRVYrdoklQi9ENhFysG7D6nqYTMgr',2,'2016-02-03 00:33:18','2016-03-11 19:28:39',NULL),(33,'root','$2y$10$sXsv2RLmah.tRJoFSvXh3e2z8YRMUDxmZe5tK9XglZ1I2y9aJTOka','2016-03-11 19:28:04','0000-00-00 00:00:00','8n3AOpaiAs98vtx6xgqmIgQ4GHfcIPWILL3QrZapyLTHWx0WKPhSNFBwAvOF',0,'2016-02-24 03:41:24','2016-03-11 19:28:04',NULL);
/*!40000 ALTER TABLE `seg_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seg_loginmodulo`
--

DROP TABLE IF EXISTS `seg_loginmodulo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seg_loginmodulo` (
  `id_segloginmodulo` int(11) NOT NULL,
  `id_seglogin` int(11) NOT NULL,
  `id_segmodulo` int(11) NOT NULL,
  `data_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_exclusao` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_segmodulo`),
  KEY `fk_id_seglogin` (`id_seglogin`),
  CONSTRAINT `fk_id_seglogin` FOREIGN KEY (`id_seglogin`) REFERENCES `seg_login` (`id_seglogin`),
  CONSTRAINT `fk_id_segmodulo` FOREIGN KEY (`id_segmodulo`) REFERENCES `seg_modulo` (`id_segmodulo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seg_loginmodulo`
--

LOCK TABLES `seg_loginmodulo` WRITE;
/*!40000 ALTER TABLE `seg_loginmodulo` DISABLE KEYS */;
/*!40000 ALTER TABLE `seg_loginmodulo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seg_modulo`
--

DROP TABLE IF EXISTS `seg_modulo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seg_modulo` (
  `id_segmodulo` int(11) NOT NULL,
  `desc_modulo` varchar(20) NOT NULL,
  `data_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_exclusao` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_segmodulo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seg_modulo`
--

LOCK TABLES `seg_modulo` WRITE;
/*!40000 ALTER TABLE `seg_modulo` DISABLE KEYS */;
/*!40000 ALTER TABLE `seg_modulo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensor`
--

DROP TABLE IF EXISTS `sensor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensor` (
  `id_sensor` int(11) NOT NULL AUTO_INCREMENT,
  `data_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_exclusao` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `desc_nome` varchar(40) NOT NULL,
  `desc_sigla` varchar(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_sensor`),
  UNIQUE KEY `desc_nome` (`desc_nome`),
  UNIQUE KEY `desc_sigla` (`desc_sigla`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensor`
--

LOCK TABLES `sensor` WRITE;
/*!40000 ALTER TABLE `sensor` DISABLE KEYS */;
INSERT INTO `sensor` VALUES (6,'2016-06-25 19:50:04','0000-00-00 00:00:00','Temperature','Uk','2016-02-01 02:29:38','2016-06-25 22:50:04',NULL),(7,'2016-06-25 19:50:28','0000-00-00 00:00:00','Humidity','AT','2016-02-06 01:39:55','2016-06-25 22:50:28',NULL);
/*!40000 ALTER TABLE `sensor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensor_ambiente_equipamento`
--

DROP TABLE IF EXISTS `sensor_ambiente_equipamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensor_ambiente_equipamento` (
  `id_sensorambienteequipamento` int(11) NOT NULL AUTO_INCREMENT,
  `data_inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_exclusao` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_ambiente` int(11) NOT NULL,
  `id_sensor` int(11) NOT NULL,
  `id_equipamento` int(11) NOT NULL,
  `id_codigosensor` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_sensorambienteequipamento`),
  KEY `sensor_ambiente_equipamento_id_ambiente_idx` (`id_ambiente`),
  KEY `sensor_ambiente_equipamento_id_sensor_idx` (`id_sensor`),
  KEY `sensor_ambiente_equipamento_id_equipamento_idx` (`id_equipamento`),
  KEY `sensor_ambiente_equipamento_id_codigosensor_idx` (`id_codigosensor`),
  CONSTRAINT `sensor_ambiente_equipamento_ibfk_1` FOREIGN KEY (`id_sensor`) REFERENCES `sensor` (`id_sensor`),
  CONSTRAINT `sensor_ambiente_equipamento_ibfk_2` FOREIGN KEY (`id_ambiente`) REFERENCES `ambiente` (`id_ambiente`),
  CONSTRAINT `sensor_ambiente_equipamento_ibfk_3` FOREIGN KEY (`id_equipamento`) REFERENCES `equipamento` (`id_equipamento`),
  CONSTRAINT `sensor_ambiente_equipamento_ibfk_4` FOREIGN KEY (`id_codigosensor`) REFERENCES `codigo_sensor` (`id_codigosensor`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensor_ambiente_equipamento`
--

LOCK TABLES `sensor_ambiente_equipamento` WRITE;
/*!40000 ALTER TABLE `sensor_ambiente_equipamento` DISABLE KEYS */;
INSERT INTO `sensor_ambiente_equipamento` VALUES (11,'2016-02-07 01:07:06','0000-00-00 00:00:00',1,6,24,1,NULL,'2016-02-06 01:38:03','2016-02-07 01:07:06'),(12,'2016-02-09 20:18:58','0000-00-00 00:00:00',25,7,25,1,NULL,'2016-02-06 01:40:03','2016-02-09 20:18:58');
/*!40000 ALTER TABLE `sensor_ambiente_equipamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sistema`
--

DROP TABLE IF EXISTS `sistema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sistema` (
  `id_sistema` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) DEFAULT NULL,
  `data_primeirouso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `qtd_sensores` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_sistema`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sistema`
--

LOCK TABLES `sistema` WRITE;
/*!40000 ALTER TABLE `sistema` DISABLE KEYS */;
/*!40000 ALTER TABLE `sistema` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-25 17:05:05
