Credenciais de usuário root padrão - root / root
(pode ser alterado no arquivo: /app/Providers/AppServiceProvider.php, na função boot)


PERMISSÕES DE USUÁRIOS QUANTO AO TIPO DE USUÁRIO
	root - ok
		# Configurar ambientes, equipamento, sensores, distribuição
		# Configurar usuários tipo administrador
		# Configurar usuários tipo regular
		# Configurar usuários tipo root
		# Acessar resumo de sensores
		# Acessar leitura de sensores
		# Acessar graficos
		# Acessar carta-controle
		# Configurar licença
		# Configurar outliers

	administrador - ok
		# Configurar ambientes, equipamento, sensores, distribuição
		# Configurar usuários tipo administrador
		# Configurar usuários tipo regular
		# Acessar resumo de sensores
		# Acessar leitura de sensores
		# Acessar graficos
		# Acessar carta-controle
		# Configurar licença
		# Configurar outliers

	regulares - ok
		# Acessar resumo de sensores
		# Acessar leitura de sensores
		# Acessar graficos

	não logados - ok
		# logar no sistema
		

PERMISSÃO QUANTO A LICENÇA
	Licença básica
		# Configurar ambientes, equipamento, sensores, distribuição
		# Configurar usuários administrador/regular/root
		# Acessar resumo de sensores
		# Acessar leitura de sensores
		# Configurar licença
		# Configurar outliers
		
	Modulo gráficos
		# Acessar graficos

	Modulo carta-controle
		# Acessar carta-controle